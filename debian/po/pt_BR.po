# Debconf translations for guacamole.
# Copyright (C) 2011 THE guacamole'S COPYRIGHT HOLDER
# This file is distributed under the same license as the guacamole package.
# Adriano Rafael Gomes <adrianorg@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: guacamole\n"
"Report-Msgid-Bugs-To: guacamole-client@packages.debian.org\n"
"POT-Creation-Date: 2013-09-23 15:49-0700\n"
"PO-Revision-Date: 2011-12-06 21:54-0200\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@gmail.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../guacamole-tomcat.templates:2001
msgid "Restart Tomcat server?"
msgstr "Reiniciar o servidor Tomcat?"

#. Type: boolean
#. Description
#: ../guacamole-tomcat.templates:2001
msgid ""
"The installation of Guacamole under Tomcat requires restarting the Tomcat "
"server, as Tomcat will only read configuration files on startup."
msgstr ""
"A instalação do Guacamole sob o Tomcat requer reiniciar o servidor Tomcat, "
"pois o Tomcat somente lerá os arquivos de configuração ao iniciar."

#. Type: boolean
#. Description
#: ../guacamole-tomcat.templates:2001
msgid ""
"You can also restart Tomcat manually by running \"invoke-rc.d tomcat8 restart"
"\" as root."
msgstr ""
"Você também pode reiniciar o Tomcat manualmente, executando \"invoke-rc.d "
"tomcat8 restart\" como root."
