angular.module('templates-main', ['app/ext/totp/templates/authenticationCodeField.html']);

angular.module('app/ext/totp/templates/authenticationCodeField.html', []).run(['$templateCache', function($templateCache) {
	$templateCache.put('app/ext/totp/templates/authenticationCodeField.html',
	"<div class=\"totp-code-field\" ng-class=\"{ 'totp-details-visible' : detailsShown }\">\n" +
	"\n" +
	"    <!-- Enroll user if necessary -->\n" +
	"    <div class=\"totp-enroll\" ng-show=\"field.qrCode\">\n" +
	"\n" +
	"        <p translate=\"TOTP.HELP_ENROLL_BARCODE\"></p>\n" +
	"\n" +
	"        <!-- Barcode and key details -->\n" +
	"        <div class=\"totp-qr-code\"><img ng-src=\"{{field.qrCode}}\" ng-click=\"openKeyURI()\"></div>\n" +
	"        <h3 class=\"totp-details-header\">\n" +
	"            {{'TOTP.SECTION_HEADER_DETAILS' | translate}}\n" +
	"            <a class=\"totp-show-details\" ng-click=\"showDetails()\">{{'TOTP.ACTION_SHOW_DETAILS' | translate}}</a>\n" +
	"            <a class=\"totp-hide-details\" ng-click=\"hideDetails()\">{{'TOTP.ACTION_HIDE_DETAILS' | translate}}</a>\n" +
	"        </h3>\n" +
	"        <table class=\"totp-details\">\n" +
	"            <tr>\n" +
	"                <th>{{'TOTP.FIELD_HEADER_SECRET_KEY' | translate}}</th>\n" +
	"                <td><span ng-repeat=\"group in groupedSecret\"\n" +
	"                          class=\"totp-detail\">{{ group }}</span></td>\n" +
	"            </tr>\n" +
	"            <tr>\n" +
	"                <th>{{'TOTP.FIELD_HEADER_DIGITS' | translate}}</th>\n" +
	"                <td><span class=\"totp-detail\">{{ field.digits }}</span></td>\n" +
	"            </tr>\n" +
	"            <tr>\n" +
	"                <th>{{'TOTP.FIELD_HEADER_ALGORITHM' | translate}}</th>\n" +
	"                <td><span class=\"totp-detail\">{{ field.mode }}</span></td>\n" +
	"            </tr>\n" +
	"            <tr>\n" +
	"                <th>{{'TOTP.FIELD_HEADER_INTERVAL' | translate}}</th>\n" +
	"                <td><span class=\"totp-detail\">{{ field.period }}</span></td>\n" +
	"            </tr>\n" +
	"        </table>\n" +
	"\n" +
	"        <p translate=\"TOTP.HELP_ENROLL_VERIFY\"\n" +
	"           translate-values=\"{ DIGITS : field.digits }\"></p>\n" +
	"\n" +
	"    </div>\n" +
	"\n" +
	"    <!-- Field for entry of the current TOTP code -->\n" +
	"    <div class=\"totp-code\">\n" +
	"        <input type=\"text\"\n" +
	"               placeholder=\"{{'TOTP.FIELD_PLACEHOLDER_CODE' |translate}}\"\n" +
	"               ng-model=\"model\" autocorrect=\"off\" autocapitalize=\"off\"/>\n" +
	"    </div>\n" +
	"\n" +
	"</div>");
}]);

