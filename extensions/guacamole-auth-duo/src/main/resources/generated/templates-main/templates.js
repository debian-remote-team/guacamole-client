angular.module('templates-main', ['app/ext/duo/templates/duoSignedResponseField.html']);

angular.module('app/ext/duo/templates/duoSignedResponseField.html', []).run(['$templateCache', function($templateCache) {
	$templateCache.put('app/ext/duo/templates/duoSignedResponseField.html',
	"<div class=\"duo-signature-response-field-container\">\n" +
	"    <div class=\"duo-signature-response-field\" ng-class=\"{ loading : !duoInterfaceLoaded }\">\n" +
	"        <iframe></iframe>\n" +
	"        <input type=\"submit\">\n" +
	"    </div>\n" +
	"</div>");
}]);

